# Название приложения: Создание категории партнеров и статуса

## Диаграмма классов
![диаграмма классов](docs/img.png)

## Описание
Данное приложение разработано на базе Spring Boot 2.7.16 с использованием Hibernate, Spring Data JPA и Spring Security с JWT-аутентификацией. Оно предназначено для удобного создания категорий партнеров и их статусов.

## Требования
Перед установкой и запуском приложения, убедитесь, что у вас установлены следующие компоненты:

- Java JDK 1.8 или выше
- Maven
- Docker
- Docker Compose
- H2
- PostgreSQL (опционально)
- MySQL (опционально)

## Установка и запуск

1. Склонируйте репозиторий с помощью следующей команды:
   ```
   git clone https://gitlab.com/VladSemenovForVibeLab/consignment-application.git
   ```

2. Перейдите в каталог проекта:
   ```
   cd consignmentapplication
   ```

3. Соберите приложение с помощью Maven:
   ```
   mvn clean package
   ```

4. Запустите контейнеры Docker с помощью Docker Compose:
   ```
   docker-compose up -d
   ```

5. Дождитесь успешного запуска контейнеров.

## Доступ к приложению

После успешного запуска, приложение будет доступно по следующему URL:

```
http://localhost:8080
```

## API Endpoints

### Аутентификация

- `POST /api/v1/auth/register` - Регистрация пользователя
- `POST /api/v1/auth/login` - Вход в систему

### Endpoints

```
В папке postman предоставлены endpoints
```

## База данных

Приложение использует PostgreSQL или MySQL для хранения данных в зависимости от вашей конфигурации Docker Compose. По умолчанию, используется PostgreSQL.

### PostgreSQL

- Хост: localhost
- Порт: 5432
- Имя пользователя: postgres
- Пароль: postgres
- Имя базы данных: partners

### MySQL

- Хост: localhost
- Порт: 3306
- Имя пользователя: root
- Пароль: root
- Имя базы данных: partners
