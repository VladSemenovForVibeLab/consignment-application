package com.consignment.consignmentapplication.service.impl;

import com.consignment.consignmentapplication.domain.User;
import com.consignment.consignmentapplication.domain.enums.Role;
import com.consignment.consignmentapplication.domain.exception.ResourceNotFoundException;
import com.consignment.consignmentapplication.repository.UserRepository;
import com.consignment.consignmentapplication.service.interf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
public class UserServiceImpl extends AbstractCRUDService<User,Long> implements UserService {

    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;
    @Override
    JpaRepository<User, Long> getRepositoryForEntity() {
        return userRepository;
    }

    @Override
    @Transactional
    public User update(User entity) {
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        userRepository.save(entity);
        return entity;

    }

    @Override
    public void create(User entity) {
        if(userRepository.findByUsername(entity.getUsername()).isPresent()){
            throw new IllegalStateException("User already exists.");
        }
        if(!entity.getPassword().equals(entity.getPasswordConfirmation())){
            throw new IllegalStateException("Password and password confirmation do not match");
        }
        entity.setPassword(passwordEncoder.encode(entity.getPassword()));
        Set<Role> roles = Set.of(Role.ROLE_USER);
        entity.setRoles(roles);
        userRepository.save(entity);
    }

    @Override
    @Transactional(readOnly = true)
    public User getByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(()->new ResourceNotFoundException("User not found"));
    }
}
