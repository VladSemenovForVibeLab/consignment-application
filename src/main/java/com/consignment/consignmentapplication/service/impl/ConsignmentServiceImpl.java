package com.consignment.consignmentapplication.service.impl;

import com.consignment.consignmentapplication.domain.Consignment;
import com.consignment.consignmentapplication.repository.ConsignmentRepository;
import com.consignment.consignmentapplication.service.interf.ConsignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;

@Service
public class ConsignmentServiceImpl extends AbstractCRUDService<Consignment,Integer> implements ConsignmentService {
   @Autowired
   private ConsignmentRepository consignmentRepository;
    @Override
    JpaRepository<Consignment, Integer> getRepositoryForEntity() {
        return consignmentRepository;
    }
}
