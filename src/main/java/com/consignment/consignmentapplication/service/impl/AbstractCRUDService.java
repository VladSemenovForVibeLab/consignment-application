package com.consignment.consignmentapplication.service.impl;

import com.consignment.consignmentapplication.service.interf.CRUDServiceForConsignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractCRUDService<E,K> implements CRUDServiceForConsignment<E,K> {
    abstract JpaRepository<E,K> getRepositoryForEntity();

    @Override
    @Transactional
    public void create(E entity) {
        getRepositoryForEntity().save(entity);
    }

    @Override
    public E findById(K id) {
        return getRepositoryForEntity().findById(id)
                .orElseThrow(()->new RuntimeException("Consignment bot found "+id));
    }

    @Override
    public List<E> findAll() {
        List<E> entities = new ArrayList<>();
        getRepositoryForEntity().findAll().forEach(entities::add);
        return entities;
    }

    @Override
    public E update(E entity) {
        getRepositoryForEntity().save(entity);
        return entity;
    }

    @Override
    public void delete(E entity) {
        getRepositoryForEntity().delete(entity);
    }
}
