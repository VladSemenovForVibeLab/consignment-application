package com.consignment.consignmentapplication.service.interf;

import com.consignment.consignmentapplication.domain.Consignment;

public interface ConsignmentService extends CRUDServiceForConsignment<Consignment,Integer> {
}
