package com.consignment.consignmentapplication.service.interf;

import java.util.List;

public interface CRUDServiceForConsignment<E,K> {
    void create(E entity);

    E findById(K id);

    List<E> findAll();

    E update(E entity);

    void delete(E entity);
}
