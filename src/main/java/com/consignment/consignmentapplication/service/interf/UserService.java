package com.consignment.consignmentapplication.service.interf;

import com.consignment.consignmentapplication.domain.User;

public interface UserService extends CRUDServiceForConsignment<User,Long> {
    User getByUsername(String username);
}
