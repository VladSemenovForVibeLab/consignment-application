package com.consignment.consignmentapplication.service.interf;

import com.consignment.consignmentapplication.web.dto.JWTRequest;
import com.consignment.consignmentapplication.web.dto.JWTResponse;

public interface AuthService {
    JWTResponse login(JWTRequest loginRequest);

    JWTResponse refresh(String refreshToken);

}
