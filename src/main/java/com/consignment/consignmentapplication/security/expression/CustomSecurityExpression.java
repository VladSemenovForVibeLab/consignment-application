package com.consignment.consignmentapplication.security.expression;

import com.consignment.consignmentapplication.domain.enums.Role;
import com.consignment.consignmentapplication.security.JwtEntity;
import com.consignment.consignmentapplication.service.interf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service("customSecurityExpression")
public class CustomSecurityExpression {
    @Autowired
    private UserService userService;

    public CustomSecurityExpression(UserService userService) {
        this.userService = userService;
    }

    public CustomSecurityExpression() {
    }
    public boolean canAccessUser(Long id){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        JwtEntity user = (JwtEntity) authentication.getPrincipal();
        Long userId = user.getId();
        return userId.equals(id) || hasAnyRole(authentication, Role.ROLE_ADMIN);
    }

    private boolean hasAnyRole(Authentication authentication, Role... roles) {
        for(Role role:roles){
            SimpleGrantedAuthority authority = new SimpleGrantedAuthority(role.name());
            if(authentication.getAuthorities().contains(authority)){
                return true;
            }
        }
        return false;
    }

}
