package com.consignment.consignmentapplication.domain;

import javax.persistence.*;

@Entity
@Table(name = "consignment")
public class Consignment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;

    private String constanmeCategory;

    private String deleveryPartner;

    private String consignmentStatus;

    public Consignment(Integer id, String constanmeCategory, String deleveryPartner, String consignmentStatus) {
        this.id = id;
        this.constanmeCategory = constanmeCategory;
        this.deleveryPartner = deleveryPartner;
        this.consignmentStatus = consignmentStatus;
    }

    public Consignment() {
    }

    public String getConsignmentStatus() {
        return consignmentStatus;
    }

    public void setConsignmentStatus(String consignmentStatus) {
        this.consignmentStatus = consignmentStatus;
    }

    public String getDeleveryPartner() {
        return deleveryPartner;
    }

    public void setDeleveryPartner(String deleveryPartner) {
        this.deleveryPartner = deleveryPartner;
    }

    public String getConstanmeCategory() {
        return constanmeCategory;
    }

    public void setConstanmeCategory(String constanmeCategory) {
        this.constanmeCategory = constanmeCategory;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}