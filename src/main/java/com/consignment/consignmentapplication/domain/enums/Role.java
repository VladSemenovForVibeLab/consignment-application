package com.consignment.consignmentapplication.domain.enums;

public enum Role {
    ROLE_USER,
    ROLE_ADMIN
}

