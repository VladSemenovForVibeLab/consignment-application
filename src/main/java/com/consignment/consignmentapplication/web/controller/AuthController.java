package com.consignment.consignmentapplication.web.controller;

import com.consignment.consignmentapplication.domain.User;
import com.consignment.consignmentapplication.service.interf.AuthService;
import com.consignment.consignmentapplication.service.interf.UserService;
import com.consignment.consignmentapplication.web.dto.JWTRequest;
import com.consignment.consignmentapplication.web.dto.JWTResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(AuthController.AUTHENTICATION_REST_CONTROLLER_URL)

public class AuthController {
    public static final String AUTHENTICATION_REST_CONTROLLER_URL="/api/v1/auth";
    @Autowired
    private AuthService authService;
    @Autowired
    private UserService userService;
    @PostMapping
    @RequestMapping("/login")
    public JWTResponse login(@Validated @RequestBody JWTRequest loginRequest){
        return authService.login(loginRequest);
    }

    @PostMapping
    @RequestMapping("/register")
    public void register(@RequestBody User userDto){
        userService.create(userDto);
    }

    @PostMapping
    @RequestMapping("/refresh")
    public JWTResponse refresh(@RequestBody String refreshToken){
        return authService.refresh(refreshToken);
    }

}
