package com.consignment.consignmentapplication.web.controller;

import com.consignment.consignmentapplication.service.interf.CRUDServiceForConsignment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class AbstractCRUDRestController<E,K> {
    abstract CRUDServiceForConsignment<E,K> getServiceForEntity();

    @PostMapping
    public ResponseEntity<E> create(@RequestBody E entity){
        getServiceForEntity().create(entity);
        return ResponseEntity.ok(entity);
    }
    @GetMapping("/{id}")
    public ResponseEntity<E> findById(@PathVariable K id){
        E entity = getServiceForEntity().findById(id);
        if(entity==null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(entity);
    }
    @GetMapping
    public ResponseEntity<List<E>> findAll(){
        List<E> entities = getServiceForEntity().findAll();
        return ResponseEntity.ok(entities);
    }
    @PutMapping
    public ResponseEntity<E> update(@RequestBody E entity){
        E updateEntity = getServiceForEntity().update(entity);
        return ResponseEntity.ok(updateEntity);
    }
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable K id){
        E entityForDelete = getServiceForEntity().findById(id);
        getServiceForEntity().delete(entityForDelete);
        return ResponseEntity.notFound().build();
    }
}
