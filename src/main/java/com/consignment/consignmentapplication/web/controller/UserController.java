package com.consignment.consignmentapplication.web.controller;

import com.consignment.consignmentapplication.domain.Consignment;
import com.consignment.consignmentapplication.domain.User;
import com.consignment.consignmentapplication.service.interf.CRUDServiceForConsignment;
import com.consignment.consignmentapplication.service.interf.ConsignmentService;
import com.consignment.consignmentapplication.service.interf.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(UserController.USER_REST_CONTROLLER_URL)
public class UserController extends AbstractCRUDRestController<User,Long>{
    public static final String USER_REST_CONTROLLER_URL="/api/v1/users";
    @Autowired
    private UserService userService;
    @Override
    CRUDServiceForConsignment<User, Long> getServiceForEntity() {
        return userService;
    }
}
