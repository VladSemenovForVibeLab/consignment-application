package com.consignment.consignmentapplication.web.controller;

import com.consignment.consignmentapplication.domain.Consignment;
import com.consignment.consignmentapplication.service.interf.CRUDServiceForConsignment;
import com.consignment.consignmentapplication.service.interf.ConsignmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*",allowedHeaders = "*")
@RestController
@RequestMapping(ConsignmentController.CONSIGNMENT_REST_CONTROLLER_URL)
public class ConsignmentController extends AbstractCRUDRestController<Consignment,Integer> {
    public static final String CONSIGNMENT_REST_CONTROLLER_URL="/api/v1/consignments";
    @Autowired
    private ConsignmentService consignmentService;
    @Override
    CRUDServiceForConsignment<Consignment, Integer> getServiceForEntity() {
        return consignmentService;
    }
}
