package com.consignment.consignmentapplication.repository;

import com.consignment.consignmentapplication.domain.Consignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ConsignmentRepository extends JpaRepository<Consignment, Integer> {
}