package com.consignment.consignmentapplication.repository;

import com.consignment.consignmentapplication.domain.User;
import com.consignment.consignmentapplication.domain.enums.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    Optional<User> findByNameAndRolesContaining(String name, Role role);

}