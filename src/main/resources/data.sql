-- Удаляем существующие записи из таблицы, если они есть
DELETE
FROM users;

-- Вставляем 10 пользователей
INSERT INTO users (id, username, name, password)
--пароль password{id}--
VALUES (1, 'user1', 'User 1', '$2a$12$aYifIJ4aqSTnsVWtRoHHreSRfhbeqXEGebvWz2gJAtaD.eLDqz8J6'),
       (2, 'user2', 'User 2', '$2a$12$Ej5fbFRbn6NgunWY9eqHbu/Akfw42ww1vnyA1UtwyYiatDpYYC/dG'),
       (3, 'user3', 'User 3', '$2a$12$OdKS9dkbpOS6VUj6E969cevfKA1NHIci0sOWNo/VjTYjgHarDMSpm'),
       (4, 'user4', 'User 4', '$2a$12$34DdhYiqQoHVcx0489D6vuSO66BuIDgDhxNqMB8dnGP2tNba.wQ8S'),
       (5, 'user5', 'User 5', '$2a$12$34DdhYiqQoHVcx0489D6vuSO66BuIDgDhxNqMB8dnGP2tNba.wQ8S'),
       --пароль adminpassword{id}
       (6, 'admin1', 'Admin 1', '$2a$12$dseSgDXs6NgBbQuUIh2MuuorUPPTQZvp.38wixRlTaJIv1yM/4jAu'),
       (7, 'admin2', 'Admin 2', '$2a$12$jm8e7aiC0dqTVG.sZGC8aOQukOdQXQMBbaI9ZWBGik66IL7m9ptTW'),
       (8, 'admin3', 'Admin 3', '$2a$12$RbPemWvIIEX7Hxgm1c4wveIAiM/K8Q6DOw5e7kQRu/jlrdrV7ibci'),
       (9, 'admin4', 'Admin 4', '$2a$12$La1XLTPebnqwL8P6vEZuhev6cmZajQCDRECGDeMaqaioxW.1cf6/G'),
       (10, 'admin5', 'Admin 5', '$2a$12$Ew2JQMskTVKVk0O3oz0lieTZ44vfAbiUt8jFzUtBvhF1mAAW4eKL.');
INSERT INTO users_roles (user_id, role)
VALUES
    (1, 'ROLE_USER'),
    (2, 'ROLE_USER'),
    (3, 'ROLE_USER'),
    (4, 'ROLE_USER'),
    (5, 'ROLE_USER'),
    (6, 'ROLE_ADMIN'),
    (7, 'ROLE_ADMIN'),
    (8, 'ROLE_ADMIN'),
    (9, 'ROLE_ADMIN'),
    (10, 'ROLE_ADMIN');
INSERT INTO consignment (constanme_category, delevery_partner, consignment_status)
VALUES ('Category 1', 'Delivery Partner 1', 'Status 1'),
       ('Category 2', 'Delivery Partner 2', 'Status 2'),
       ('Category 3', 'Delivery Partner 3', 'Status 3'),
       ('Category 4', 'Delivery Partner 4', 'Status 4'),
       ('Category 5', 'Delivery Partner 5', 'Status 5'),
       ('Category 6', 'Delivery Partner 6', 'Status 6'),
       ('Category 7', 'Delivery Partner 7', 'Status 7'),
       ('Category 8', 'Delivery Partner 8', 'Status 8'),
       ('Category 9', 'Delivery Partner 9', 'Status 9'),
       ('Category 10', 'Delivery Partner 10', 'Status 10');