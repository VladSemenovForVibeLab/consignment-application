package com.consignment.consignmentapplication;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootTest
@EnableTransactionManagement
class ConsignmentapplicationApplicationTests {

	@Test
	void contextLoads() {
	}

}
